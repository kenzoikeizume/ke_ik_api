"""empty message

Revision ID: 7a308feedf62
Revises: 5cfd1c3fe1ab
Create Date: 2019-07-09 12:39:24.429812

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7a308feedf62'
down_revision = '5cfd1c3fe1ab'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('bracket', sa.Column('octaves_id', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('bracket', 'octaves_id')
    # ### end Alembic commands ###
