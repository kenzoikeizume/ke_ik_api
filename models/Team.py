from app import db


class Team(db.Model):
    __tablename__ = 'teams'

    id = db.Column(db.Integer, primary_key=True)
    team = db.Column(db.String())
    group = db.Column(db.String())
    points = db.Column(db.Integer)
    victory = db.Column(db.Integer)

    def __init__(self, team, group, points, victory):
        self.team = team
        self.group = group
        self.points = points
        self.victory = victory

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'team': self.team,
            'group': self.group,
            'points': self.points,
            'victory': self.victory
        }
