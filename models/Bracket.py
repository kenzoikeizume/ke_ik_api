from app import db


class Bracket(db.Model):
    __tablename__ = 'bracket'

    id = db.Column(db.Integer, primary_key=True)
    quarter_id = db.Column(db.Integer)
    quarter_rival_id = db.Column(db.Integer)
    semi_id = db.Column(db.Integer)
    semi_final_id = db.Column(db.Integer)
    final_id = db.Column(db.Integer)
    final_rival_id = db.Column(db.Integer)
    winner_id = db.Column(db.Integer)

    def __init__(self, quarter_id=None, quarter_rival_id=None, semi_id=None, semi_final_id=None,
                 final_id=None, final_rival_id=None, winner_id=None):
        self.quarter_id = quarter_id
        self.quarter_rival_id = quarter_rival_id
        self.semi_id = semi_id
        self.semi_final_id = semi_final_id
        self.final_id = final_id
        self.final_rival_id = final_rival_id
        self.winner_id = winner_id

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'quarter_id': self.quarter_id,
            'quarter_rival_id': self.quarter_rival_id,
            'semi_id': self.semi_id,
            'semi_final_id': self.semi_final_id,
            'final_id': self.final_id,
            'final_rival_id': self.final_rival_id,
            'winner_id': self.winner_id
        }
