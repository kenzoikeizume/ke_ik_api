from app import db


class Playoffs(db.Model):
    __tablename__ = 'playoffs'

    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.String())
    victory = db.Column(db.Boolean, default=None)

    def __init__(self, team_id, victory=None):
        self.team_id = team_id
        self.victory = victory

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'team_id': self.team_id,
            'victory': self.victory
        }