### `requirements`
pip install requirements.txt

### `environment develop`
Set your virtual environment to develop mode

set APP_SETTINGS=config.DevelopmentConfig
set DATABASE_URL=postgresql://postgres:admin@localhost:5432/championship

### `set environment`
heroku config:set APP_SETTINGS=config.ProductionConfig --remote prod

### `flask run`

Runs the api in the development mode.
Open [http://localhost:5000](http://localhost:5000) to view it in the browser.
