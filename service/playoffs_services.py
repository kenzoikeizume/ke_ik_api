import random

from models.Playoffs import Playoffs
from repository.playoffs_repository import PlayoffsRepository


class PlayoffsService:

    def __init__(self, db):
        self._playoffs_repository = PlayoffsRepository(db)

    def add_teams_playoffs(self, teams, group_teams):
        try:
            for group in group_teams:
                order_list = sorted(teams[group], key=lambda x: (x.victory, x.points), reverse=True)

                team_1 = Playoffs(
                    team_id=order_list[0].id
                )

                team_2 = Playoffs(
                    team_id=order_list[1].id
                )

                self._playoffs_repository.add_playoffs(team_1)
                self._playoffs_repository.add_playoffs(team_2)

            return 'ok'
        except Exception as e:
            return str(e)

    def get_all(self):
        try:
            teams = self._playoffs_repository.get_all_playoffs()

            return [e.serialize() for e in teams]
        except Exception as e:
            return str(e)

    def delete_all_teams(self):
        try:
            num_rows_deleted = self._playoffs_repository.delete_all_teams_playoffs()

            return num_rows_deleted
        except Exception as e:
            return str(e)

    def play_playoffs(self, fist_team, last_team):
        try:
            f_team = self._playoffs_repository.get_by_id(fist_team['id'])
            l_team = self._playoffs_repository.get_by_id(last_team['id'])

            if random.choice([True, False]):
                f_team.victory = True
                l_team.victory = False
            else:
                f_team.victory = True
                l_team.victory = False

            print(f_team)
            print(l_team)

            self._playoffs_repository.update_playoffs(f_team)
            self._playoffs_repository.update_playoffs(l_team)

            return 'ok'
        except Exception as e:
            return str(e)
