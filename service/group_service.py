import random
from repository.team_repository import TeamRepository
from service.playoffs_services import PlayoffsService


class GroupService:

    def __init__(self, db):
        self._team_repository = TeamRepository(db=db)
        self.playoffs_service = PlayoffsService(db=db)

    def play_groups_phase(self):
        try:
            teams = self._team_repository.get_all()

            teams_phase = self.split_for_group(teams)

            result_games = self.get_result(teams_phase)

            self.updates_teams(result_games, teams_phase[1])

            self.playoffs_service.add_teams_playoffs(teams=result_games, group_teams=teams_phase[1])

            return 'ok'
        except Exception as e:
            return str(e)

    @staticmethod
    def split_for_group(teams):
        groups_teams = {}
        groups = []

        for team in teams:
            if team.group in groups_teams:
                groups_teams[team.group].append(team)
            else:
                groups.append(team.group)
                groups_teams[team.group] = [team]

        return [groups_teams, groups]

    @staticmethod
    def get_result(teams_phase):
        [groups_teams, groups] = teams_phase

        for group in groups:
            list_teams = groups_teams[group]

            for i_index in range(0, len(list_teams) - 1):
                for j_index in range(i_index + 1, len(list_teams)):
                    points_a = 0
                    points_b = 0

                    while points_a < 16 or points_b < 16:
                        if random.choice([True, False]):
                            points_a = points_a + 1
                        else:
                            points_b = points_b + 1

                    if points_a > points_b:
                        groups_teams[group][i_index].victory = groups_teams[group][i_index].victory + 1
                        groups_teams[group][i_index].points = points_a - points_b
                        groups_teams[group][j_index].points = points_b - points_a
                    else:
                        groups_teams[group][j_index].victory = groups_teams[group][j_index].victory + 1
                        groups_teams[group][j_index].points = points_b - points_a
                        groups_teams[group][i_index].points = points_a - points_b

        return groups_teams

    def updates_teams(self, groups_teams, groups):
        for group in groups:
            for team in groups_teams[group]:
                self._team_repository.update_team(team)





