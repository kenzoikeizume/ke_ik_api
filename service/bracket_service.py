import random

from models.Bracket import Bracket
from repository.bracket_repository import BracketRepository
from repository.playoffs_repository import PlayoffsRepository


class BracketService:

    def __init__(self, db):
        self._bracket_repository = BracketRepository(db)
        self._playoffs_repository = PlayoffsRepository(db)

    def add_team_bracket(self):
        try:
            teams_playoffs = self._playoffs_repository.get_all_playoffs()
            get_victories_team = [x for x in teams_playoffs if x.victory]
            order_list = sorted(get_victories_team, key=lambda x: x.id, reverse=True)
            random.shuffle(order_list)

            for i in range(0, len(order_list), 2):
                team = order_list[i]
                team_rival = order_list[i + 1]

                bracket = Bracket(
                    quarter_id=team.team_id,
                    quarter_rival_id=team_rival.team_id
                )

                self._bracket_repository.add_team(bracket)

            return 'ok'
        except Exception as e:
            return str(e)

    def play_quarters_bracket(self):
        try:
            brackets = self._bracket_repository.get_all()
            order_brackets = sorted(brackets, key=lambda x: x.id, reverse=True)

            for bracket in order_brackets:
                team_a = bracket.quarter_id
                team_b = bracket.quarter_rival_id

                if random.choice([True, False]):
                    bracket.semi_id = team_a
                else:
                    bracket.semi_id = team_b

            for i in range(0, len(order_brackets), 2):
                bracket_a = order_brackets[i]
                bracket_b = order_brackets[i + 1]

                bracket_a.semi_final_id = bracket_b.semi_id

            for bracket in order_brackets:
                self._bracket_repository.add_team(bracket)

            return 'ok'

        except Exception as e:
            return str(e)

    def play_semi_final_bracket(self):
        try:
            brackets = self._bracket_repository.get_all()
            order_brackets = sorted(brackets, key=lambda x: x.id, reverse=True)
            finals = []

            for bracket in order_brackets:
                team_a = bracket.semi_id
                team_b = bracket.semi_final_id

                if team_a and team_b:
                    finals.append(team_a)
                    finals.append(team_b)

            for i in range(0, len(brackets), 2):
                team_id_a = finals[i]
                team_id_b = finals[i + 1]

                bracket_a = [x for x in order_brackets if x.semi_id == team_id_a][0]
                bracket_b = [x for x in order_brackets if x.semi_final_id == team_id_b][0]

                if random.choice([True, False]):
                    bracket_a.final_id = team_id_a
                else:
                    bracket_b.final_id = team_id_b

            filtered_brackets = [x for x in order_brackets if x.final_id]

            for i in range(0, len(filtered_brackets), 2):
                bracket_a = filtered_brackets[i]
                bracket_b = filtered_brackets[i + 1]

                bracket_a.final_rival_id = bracket_b.final_id

            for bracket in order_brackets:
                self._bracket_repository.add_team(bracket)

            return 'ok'

        except Exception as e:
            return str(e)

    def play_finals_bracket(self):
        try:
            brackets = self._bracket_repository.get_all()

            finals = [x for x in brackets if x.final_rival_id]

            bracket_a = finals[0]
            bracket_b = finals[1]

            if random.choice([True, False]):
                bracket_a.winner_id = bracket_a.final_rival_id
                self._bracket_repository.add_team(bracket_a)
            else:
                bracket_b.winner_id = bracket_b.final_rival_id
                self._bracket_repository.add_team(bracket_b)

            return 'ok'

        except Exception as e:
            return str(e)

    def get_all_teams_bracket(self):
        try:
            brackets = self._bracket_repository.get_all()

            return [e.serialize() for e in brackets]
        except Exception as e:
            return str(e)

    def delete_all_teams_bracket(self):
        try:
            num_rows_deleted = self._bracket_repository.delete_all_teams()

            return num_rows_deleted
        except Exception as e:
            return str(e)
