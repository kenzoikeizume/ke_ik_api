from models.Team import Team
from repository.team_repository import TeamRepository


class TeamService:

    def __init__(self, db):
        self._team_repository = TeamRepository(db)

    def add_team(self, team, group, points, victory):
        try:
            team = Team(
                team=team,
                group=group,
                points=points,
                victory=victory
            )

            self._team_repository.add_team(team)

            return team.id
        except Exception as e:
            return str(e)

    def get_all(self):
        try:
            teams = self._team_repository.get_all()

            return [e.serialize() for e in teams]
        except Exception as e:
            return str(e)

    def get_by_id(self, id_):
        try:
            team = self._team_repository.get_by_id(id_)

            return team.serialize()
        except Exception as e:
            return str(e)

    def delete_all_teams(self):
        try:
            num_rows_deleted = self._team_repository.delete_all_teams()

            return num_rows_deleted
        except Exception as e:
            return str(e)
