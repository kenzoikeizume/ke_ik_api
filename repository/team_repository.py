from models.Team import Team


class TeamRepository:

    def __init__(self, db):
        self._db = db

    def add_team(self, team: Team):
        try:
            self._db.session.add(team)
            self._db.session.commit()

        except Exception as e:
            return str(e)

    @staticmethod
    def get_all():
        try:
            teams = Team.query.all()

            return teams
        except Exception as e:
            return str(e)

    @staticmethod
    def get_by_id(id_):
        try:
            team = Team.query.filter_by(id=id_).first()
            return team

        except Exception as e:
            return str(e)

    def delete_all_teams(self):
        try:
            num_rows_deleted = self._db.session.query(Team).delete()
            self._db.session.commit()
            return num_rows_deleted
        except Exception as e:
            return str(e)

    def update_team(self, team):
        try:
            team = self._db.session.query(Team).filter_by(id=team.id).first()
            team.points = team.points
            team.victory = team.victory
            self._db.session.commit()
        except Exception as e:
            return str(e)
