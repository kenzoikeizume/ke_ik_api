from models.Playoffs import Playoffs


class PlayoffsRepository:

    def __init__(self, db):
        self._db = db

    def add_playoffs(self, playoffs: Playoffs):
        try:
            self._db.session.add(playoffs)
            self._db.session.commit()

        except Exception as e:
            return str(e)

    def delete_all_teams_playoffs(self):
        try:
            num_rows_deleted = self._db.session.query(Playoffs).delete()
            self._db.session.commit()
            return num_rows_deleted
        except Exception as e:
            return str(e)

    @staticmethod
    def get_all_playoffs():
        try:
            teams = Playoffs.query.all()

            return teams
        except Exception as e:
            return str(e)

    @staticmethod
    def get_by_id(id_):
        try:
            team = Playoffs.query.filter_by(id=id_).first()
            return team

        except Exception as e:
            return str(e)

    def update_playoffs(self, team):
        try:
            team = self._db.session.query(Playoffs).filter_by(id=team.id).first()
            team.victory = team.victory
            self._db.session.commit()
        except Exception as e:
            return str(e)
