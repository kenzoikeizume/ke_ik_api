from models.Bracket import Bracket


class BracketRepository:

    def __init__(self, db):
        self._db = db

    def add_team(self, bracket: Bracket):
        try:
            self._db.session.add(bracket)
            self._db.session.commit()

        except Exception as e:
            return str(e)

    @staticmethod
    def get_all():
        try:
            bracket = Bracket.query.all()

            return bracket
        except Exception as e:
            return str(e)

    def delete_all_teams(self):
        try:
            num_rows_deleted = self._db.session.query(Bracket).delete()
            self._db.session.commit()
            return num_rows_deleted
        except Exception as e:
            return str(e)

    def update_team(self, bracket):
        try:
            bracket = self._db.session.query(Bracket).filter_by(id=bracket.id).first()
            bracket.octaves_id = bracket.octaves_id
            bracket.quarter_id = bracket.quarter_id
            bracket.quarter_rival_id = bracket.quarter_rival_id
            bracket.semi_id = bracket.semi_id
            bracket.semi_final_id = bracket.semi_final_id
            bracket.final_id = bracket.final_id
            bracket.final_rival_id = bracket.final_rival_id
            bracket.winner_id = bracket.winner_id
            self._db.session.commit()
        except Exception as e:
            return str(e)
