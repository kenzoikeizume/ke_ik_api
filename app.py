import os
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS


app = Flask(__name__, static_folder='./tmp')
CORS(app, resources={r"/*": {"origins": [
    "https://dxckib064ywjd.cloudfront.net"
]}})

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

from service.team_service import TeamService
from service.group_service import GroupService
from service.playoffs_services import PlayoffsService
from service.bracket_service import BracketService


@app.route("/")
def hello():
    return "Ok!"


@app.route("/add_team", methods=['POST'])
def add_team():
    team_service = TeamService(db=db)

    content = request.json

    team = content['team']
    group = content['group']
    points = content['points']
    victory = content['victory']

    team_id = team_service.add_team(team=team,
                                    group=group,
                                    points=points,
                                    victory=victory)

    return jsonify({"response": "Team added. team id={}".format(team_id)})


@app.route("/teams", methods=['GET'])
def get_all():
    team_service = TeamService(db=db)
    teams = team_service.get_all()

    return jsonify(teams)


@app.route("/team/<id_>", methods=['GET'])
def get_by_id(id_):
    team_service = TeamService(db=db)
    team = team_service.get_by_id(id_)

    return jsonify(team)


@app.route("/teams", methods=['DELETE'])
def delete_all_teams():
    team_service = TeamService(db=db)
    rows = team_service.delete_all_teams()

    return jsonify({"response": "Rows deleted ({})".format(rows)})


@app.route("/play_groups", methods=['GET'])
def play_groups():
    group_service = GroupService(db=db)
    group = group_service.play_groups_phase()

    return jsonify(group)


@app.route("/playoffs", methods=['DELETE'])
def delete_all_playoffs():
    playoffs_service = PlayoffsService(db=db)
    rows = playoffs_service.delete_all_teams()

    return jsonify(rows)


@app.route("/playoffs", methods=['GET'])
def get_all_playoffs():
    team_service = PlayoffsService(db=db)
    teams = team_service.get_all()

    return jsonify(teams)


@app.route("/play_playoffs", methods=['POST'])
def play_playoffs():
    playoffs_service = PlayoffsService(db=db)

    content = request.json

    fist_team = content['fistTeam']
    last_team = content['lastTeam']

    group = playoffs_service.play_playoffs(fist_team=fist_team, last_team=last_team)

    return jsonify(group)


@app.route("/bracket", methods=['GET'])
def get_all_brackets():
    bracket_service = BracketService(db=db)
    brackets = bracket_service.get_all_teams_bracket()

    return jsonify(brackets)


@app.route("/add_brackets", methods=['GET'])
def add_teams_bracket():
    bracket_service = BracketService(db=db)
    rows = bracket_service.add_team_bracket()

    return jsonify(rows)


@app.route("/bracket", methods=['DELETE'])
def delete_all_bracket():
    bracket_service = BracketService(db=db)
    rows = bracket_service.delete_all_teams_bracket()

    return jsonify(rows)


@app.route("/quarter_bracket", methods=['GET'])
def quarter_bracket():
    bracket_service = BracketService(db=db)
    status = bracket_service.play_quarters_bracket()

    return jsonify(status)


@app.route("/semi_bracket", methods=['GET'])
def semi_bracket():
    bracket_service = BracketService(db=db)
    status = bracket_service.play_semi_final_bracket()

    return jsonify(status)


@app.route("/final_bracket", methods=['GET'])
def final_bracket():
    bracket_service = BracketService(db=db)
    status = bracket_service.play_finals_bracket()

    return jsonify(status)


if __name__ == '__main__':
    app.run()
